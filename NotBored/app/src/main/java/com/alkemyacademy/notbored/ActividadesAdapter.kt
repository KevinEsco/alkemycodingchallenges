package com.alkemyacademy.notbored

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

internal class ActividadesAdapter(private val data: List<String>) : RecyclerView.Adapter<ActividadesAdapter.MyViewHolder>() {
        internal inner class MyViewHolder(view: View, listener:onItemClickListener) : RecyclerView.ViewHolder(view) {
            var itemTextView: TextView = view.findViewById(R.id.itemTextView)
            init {
                itemView.setOnClickListener(){
                    listener.onItemClick(adapterPosition)
                }
            }
        }
    private lateinit var mListener: onItemClickListener

    interface onItemClickListener {
        fun onItemClick(position: Int){

        }
    }

    fun setOnItemClickListener(listener: onItemClickListener){
        mListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.actitividades_item, parent, false)
        return  MyViewHolder(itemView, mListener)
    }


    override fun getItemCount() = data.size
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = data[position]
        holder.itemTextView.text = item
    }
}