package com.alkemyacademy.notbored

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageButton
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class Actividades : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_actividades)


        var participants: Int = intent.getIntExtra("Participants", 1);
        val categories = ArrayList<String>()
        lateinit var ActividadesAdapter: ActividadesAdapter
        val rvActividades: RecyclerView = findViewById(R.id.rvActividades)
        val layoutManager = LinearLayoutManager(applicationContext)
        var btnRandom : ImageButton = findViewById(R.id.btnRandom)
        ActividadesAdapter = ActividadesAdapter(categories)
        rvActividades.layoutManager = layoutManager
        rvActividades.adapter = ActividadesAdapter

        categories.add("Education")
        categories.add("Recreational")
        categories.add("Social")
        categories.add("Diy")
        categories.add("Cooking")
        categories.add("Relaxation")
        categories.add("Music")
        categories.add("Busywork")
        categories.add("Charity")

        ActividadesAdapter.notifyDataSetChanged()
        ActividadesAdapter.setOnItemClickListener(object : ActividadesAdapter.onItemClickListener{
            override fun onItemClick(position:Int){
                val intent = Intent(this@Actividades, Sugerencia::class.java).apply {
                    putExtra("Participants",participants)
                    putExtra("Category", categories[position])
                }
                startActivity(intent)
            }
        })
        btnRandom.setOnClickListener(){
            val intent = Intent(this@Actividades, Sugerencia::class.java).apply {
                putExtra("Participants",participants)
                putExtra("Category", "Random")
            }
            startActivity(intent)
        }
    }


}