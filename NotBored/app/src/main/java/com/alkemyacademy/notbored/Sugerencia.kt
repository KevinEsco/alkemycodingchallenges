package com.alkemyacademy.notbored

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.widget.Toolbar

class Sugerencia : AppCompatActivity() {
    private lateinit var tvDescSugerencia: TextView
    private lateinit var tvParticipantsNumb: TextView
    private lateinit var tvPrice: TextView
    var SugerenciasArrList: ArrayList<Sugerencias> = arrayListOf()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sugerencia)

        var tbSugerencia: Toolbar = findViewById(R.id.tbSugerencia)
         tvParticipantsNumb= findViewById(R.id.tvParticipantsNumb)

        tvPrice= findViewById(R.id.tvPrice)
        tvDescSugerencia = findViewById(R.id.tvDescSugerencia)
        setSupportActionBar(tbSugerencia)
        //intent get extra
        var participants: Int = intent.getIntExtra("Participants", 1);
        tvParticipantsNumb.setText(participants.toString())
        var category: String? = intent.getStringExtra("Category")
        tvPrice.setText("Medium")

        supportActionBar?.apply {
            title = category
            // show back button on toolbar
            // on back button press, it will navigate to parent activity
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
        }
        val categories = ArrayList<String>()
        categories.add("Education")
        categories.add("Recreational")
        categories.add("Social")
        categories.add("Diy")
        categories.add("Cooking")
        categories.add("Relaxation")
        categories.add("Music")
        categories.add("Busywork")
        categories.add("Charity")
        cargarSugerencias(categories)
        seleccionarSugerencia(participants, if (category.equals("Random")) categories[(0..categories.size-1).random()] else category)
    }

    private fun cargarSugerencias(categories: ArrayList<String>) {

        var SugerenciasCargar: ArrayList<Sugerencias> = arrayListOf()
        val prices : Array<String> = arrayOf("Cheap","Medium", "Expensive")
        for (category in categories) {
            for (i in 1..10) {
                var nuevaSugerencia = Sugerencias(category,i,prices[(0..2).random()],"Actividad de $category para $i participantes")
//                nuevaSugerencia.Category =
//                nuevaSugerencia.Participants = i
//                nuevaSugerencia.Price = prices[(0..3).random()]
//                nuevaSugerencia.Description = "Actividad de $category para $i participantes"
                SugerenciasCargar.add(nuevaSugerencia)
            }
        }

        this.SugerenciasArrList = SugerenciasCargar
    }

    private fun seleccionarSugerencia(participants: Int, category: String?) {

        SugerenciasArrList.find{it.Category.equals(category) && it.Participants === participants}
            ?.let { mostrarSugerencia(it) }

    }
    private fun mostrarSugerencia(sugerenciaMostrar:Sugerencias) {
        //mostrar en activity tvDescripcion sugerencia el descSugerencia
        //mostrar en
        this.tvDescSugerencia.setText(sugerenciaMostrar.Description)
        this.tvParticipantsNumb.setText(sugerenciaMostrar.Participants.toString())
        this.tvPrice.setText(sugerenciaMostrar.Price)

    }
    data class Sugerencias(var Category: String, var Participants: Int, var Price: String, var Description: String){

    }

}




