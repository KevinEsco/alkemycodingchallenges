package com.alkemyacademy.notbored

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.widget.ImageButton
import android.widget.TextView


class TermsAndConditions : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_terms_and_conditions)

        var tvTermsAndConditions: TextView = findViewById(R.id.tvTermsAndCondTitulo)
        var btnClose: ImageButton = findViewById(R.id.btnClose)
        btnClose.setOnClickListener() {
            finish()
        }

        tvTermsAndConditions.movementMethod = ScrollingMovementMethod()
    }
}