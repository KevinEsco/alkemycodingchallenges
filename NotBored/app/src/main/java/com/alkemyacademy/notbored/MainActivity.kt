package com.alkemyacademy.notbored

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.widget.addTextChangedListener
import java.util.Objects.isNull

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var Participants: Int = 0
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.activity_main)
        var btnStart: Button = findViewById(R.id.btnStart)
        var btnTerms: Button = findViewById(R.id.btnTerms)
        var etParticipants: EditText = findViewById(R.id.etParticipants)
        btnStart.setOnClickListener() {
            val intent = Intent(this, Actividades::class.java).apply {
                putExtra("Participants",etParticipants.text.toString().toInt())
            }
            startActivity(intent)
        }
        btnTerms.setOnClickListener() {
            val intent = Intent(this, TermsAndConditions::class.java)
            startActivity(intent)
        }
        etParticipants.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var Participants: String = if(etParticipants.text.toString().isNotEmpty()) etParticipants.text.toString() else "-1"
                if (  Participants.toInt() < 1) {
                    btnStart.isEnabled = false
                } else {
                    if (btnStart.isEnabled === false) btnStart.isEnabled = true
                }


            }

            override fun afterTextChanged(s: Editable?) {
            }

        })


    }


}