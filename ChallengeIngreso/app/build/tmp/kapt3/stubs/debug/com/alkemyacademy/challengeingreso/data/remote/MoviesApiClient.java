package com.alkemyacademy.challengeingreso.data.remote;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0005\u001a\u00020\u0004J\b\u0010\u0006\u001a\u00020\u0007H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"Lcom/alkemyacademy/challengeingreso/data/remote/MoviesApiClient;", "", "()V", "serviceApiInterface", "Lcom/alkemyacademy/challengeingreso/data/remote/IServiceApiClient;", "build", "interceptor", "Lokhttp3/logging/HttpLoggingInterceptor;", "app_debug"})
public final class MoviesApiClient {
    @org.jetbrains.annotations.NotNull()
    public static final com.alkemyacademy.challengeingreso.data.remote.MoviesApiClient INSTANCE = null;
    private static com.alkemyacademy.challengeingreso.data.remote.IServiceApiClient serviceApiInterface;
    
    private MoviesApiClient() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.alkemyacademy.challengeingreso.data.remote.IServiceApiClient build() {
        return null;
    }
    
    private final okhttp3.logging.HttpLoggingInterceptor interceptor() {
        return null;
    }
}