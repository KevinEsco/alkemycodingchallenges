package com.alkemyacademy.challengeingreso.domain.usecases;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\nJ\u0017\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\f0\bH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\nJ/\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000e0\b2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0012\u001a\u00020\u0010H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0013R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0014"}, d2 = {"Lcom/alkemyacademy/challengeingreso/domain/usecases/MoviesUseCases;", "", "remoteRepository", "Lcom/alkemyacademy/challengeingreso/data/remote/repository/RemoteRepository;", "(Lcom/alkemyacademy/challengeingreso/data/remote/repository/RemoteRepository;)V", "getRemoteRepository", "()Lcom/alkemyacademy/challengeingreso/data/remote/repository/RemoteRepository;", "getGuestId", "Lcom/alkemyacademy/challengeingreso/domain/ObjectResult;", "Lcom/alkemyacademy/challengeingreso/data/remote/entities/responses/ResponseSessionId;", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getPopularMovies", "Lcom/alkemyacademy/challengeingreso/domain/entities/ResponseAppApi;", "postRating", "Lcom/alkemyacademy/challengeingreso/data/remote/entities/responses/ResponseRating;", "id", "", "body", "guestId", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public final class MoviesUseCases {
    @org.jetbrains.annotations.NotNull()
    private final com.alkemyacademy.challengeingreso.data.remote.repository.RemoteRepository remoteRepository = null;
    
    public MoviesUseCases(@org.jetbrains.annotations.NotNull()
    com.alkemyacademy.challengeingreso.data.remote.repository.RemoteRepository remoteRepository) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.alkemyacademy.challengeingreso.data.remote.repository.RemoteRepository getRemoteRepository() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getPopularMovies(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.alkemyacademy.challengeingreso.domain.ObjectResult<com.alkemyacademy.challengeingreso.domain.entities.ResponseAppApi>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object postRating(@org.jetbrains.annotations.NotNull()
    java.lang.String id, @org.jetbrains.annotations.NotNull()
    java.lang.String body, @org.jetbrains.annotations.NotNull()
    java.lang.String guestId, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.alkemyacademy.challengeingreso.domain.ObjectResult<com.alkemyacademy.challengeingreso.data.remote.entities.responses.ResponseRating>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getGuestId(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.alkemyacademy.challengeingreso.domain.ObjectResult<com.alkemyacademy.challengeingreso.data.remote.entities.responses.ResponseSessionId>> continuation) {
        return null;
    }
}