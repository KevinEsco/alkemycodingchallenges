package com.alkemyacademy.challengeingreso.presentation.ui;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0015\u001a\u00020\u0016H\u0002J\b\u0010\u0017\u001a\u00020\u0016H\u0002J\u0016\u0010\u0018\u001a\u00020\u00162\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\r0\u001aH\u0002J\b\u0010\u001b\u001a\u00020\u0016H\u0002J\u0012\u0010\u001c\u001a\u00020\u00162\b\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u0014J\b\u0010\u001f\u001a\u00020\u0016H\u0002J\b\u0010 \u001a\u00020\u0016H\u0002R\u001b\u0010\u0003\u001a\u00020\u00048BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0007\u0010\b\u001a\u0004\b\u0005\u0010\u0006R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\fX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\r0\fX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\r0\fX\u0082.\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0010\u001a\u00020\u00118BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0014\u0010\b\u001a\u0004\b\u0012\u0010\u0013\u00a8\u0006!"}, d2 = {"Lcom/alkemyacademy/challengeingreso/presentation/ui/MainActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "()V", "adapter", "Lcom/alkemyacademy/challengeingreso/presentation/ui/MoviesAdapter;", "getAdapter", "()Lcom/alkemyacademy/challengeingreso/presentation/ui/MoviesAdapter;", "adapter$delegate", "Lkotlin/Lazy;", "binding", "Lcom/alkemyacademy/challengeingreso/databinding/ActivityMainBinding;", "newArrayList", "", "Lcom/alkemyacademy/challengeingreso/domain/entities/Movie;", "newArrayListG", "tempArrayList", "viewModel", "Lcom/alkemyacademy/challengeingreso/presentation/viewmodel/ViewModelDemo;", "getViewModel", "()Lcom/alkemyacademy/challengeingreso/presentation/viewmodel/ViewModelDemo;", "viewModel$delegate", "bind", "", "callViewModelMethods", "filterViewmodel", "filteredMovies", "", "initRecycler", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "reloadViewodel", "viewModelObservers", "app_debug"})
public final class MainActivity extends androidx.appcompat.app.AppCompatActivity {
    private com.alkemyacademy.challengeingreso.databinding.ActivityMainBinding binding;
    private java.util.List<com.alkemyacademy.challengeingreso.domain.entities.Movie> tempArrayList;
    private java.util.List<com.alkemyacademy.challengeingreso.domain.entities.Movie> newArrayList;
    private java.util.List<com.alkemyacademy.challengeingreso.domain.entities.Movie> newArrayListG;
    private final kotlin.Lazy viewModel$delegate = null;
    private final kotlin.Lazy adapter$delegate = null;
    
    public MainActivity() {
        super();
    }
    
    private final com.alkemyacademy.challengeingreso.presentation.viewmodel.ViewModelDemo getViewModel() {
        return null;
    }
    
    private final com.alkemyacademy.challengeingreso.presentation.ui.MoviesAdapter getAdapter() {
        return null;
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void reloadViewodel() {
    }
    
    private final void filterViewmodel(java.util.List<com.alkemyacademy.challengeingreso.domain.entities.Movie> filteredMovies) {
    }
    
    private final void bind() {
    }
    
    private final void viewModelObservers() {
    }
    
    private final void callViewModelMethods() {
    }
    
    private final void initRecycler() {
    }
}