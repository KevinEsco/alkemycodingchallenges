package com.alkemyacademy.challengeingreso.presentation.ui;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\f\u0012\b\u0012\u00060\u0002R\u00020\u00000\u0001:\u0001 B\u0005\u00a2\u0006\u0002\u0010\u0003J\b\u0010\u0017\u001a\u00020\u0018H\u0016J\u001c\u0010\u0019\u001a\u00020\u00112\n\u0010\u001a\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u001b\u001a\u00020\u0018H\u0016J\u001c\u0010\u001c\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u0018H\u0016R0\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR=\u0010\f\u001a%\u0012\u0013\u0012\u00110\u0006\u00a2\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0010\u0012\u0004\u0012\u00020\u0011\u0018\u00010\rj\u0004\u0018\u0001`\u0012X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016\u00a8\u0006!"}, d2 = {"Lcom/alkemyacademy/challengeingreso/presentation/ui/MoviesAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/alkemyacademy/challengeingreso/presentation/ui/MoviesAdapter$MoviesViewHolder;", "()V", "value", "", "Lcom/alkemyacademy/challengeingreso/domain/entities/Movie;", "list", "getList", "()Ljava/util/List;", "setList", "(Ljava/util/List;)V", "movieClick", "Lkotlin/Function1;", "Lkotlin/ParameterName;", "name", "movie", "", "Lcom/alkemyacademy/challengeingreso/presentation/ui/MovieClick;", "getMovieClick", "()Lkotlin/jvm/functions/Function1;", "setMovieClick", "(Lkotlin/jvm/functions/Function1;)V", "getItemCount", "", "onBindViewHolder", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "MoviesViewHolder", "app_debug"})
public final class MoviesAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.alkemyacademy.challengeingreso.presentation.ui.MoviesAdapter.MoviesViewHolder> {
    @org.jetbrains.annotations.NotNull()
    private java.util.List<com.alkemyacademy.challengeingreso.domain.entities.Movie> list;
    @org.jetbrains.annotations.Nullable()
    private kotlin.jvm.functions.Function1<? super com.alkemyacademy.challengeingreso.domain.entities.Movie, kotlin.Unit> movieClick;
    
    public MoviesAdapter() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.alkemyacademy.challengeingreso.domain.entities.Movie> getList() {
        return null;
    }
    
    public final void setList(@org.jetbrains.annotations.NotNull()
    java.util.List<com.alkemyacademy.challengeingreso.domain.entities.Movie> value) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final kotlin.jvm.functions.Function1<com.alkemyacademy.challengeingreso.domain.entities.Movie, kotlin.Unit> getMovieClick() {
        return null;
    }
    
    public final void setMovieClick(@org.jetbrains.annotations.Nullable()
    kotlin.jvm.functions.Function1<? super com.alkemyacademy.challengeingreso.domain.entities.Movie, kotlin.Unit> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.alkemyacademy.challengeingreso.presentation.ui.MoviesAdapter.MoviesViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.alkemyacademy.challengeingreso.presentation.ui.MoviesAdapter.MoviesViewHolder holder, int position) {
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lcom/alkemyacademy/challengeingreso/presentation/ui/MoviesAdapter$MoviesViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "binding", "Lcom/alkemyacademy/challengeingreso/databinding/ItemMovieBinding;", "(Lcom/alkemyacademy/challengeingreso/presentation/ui/MoviesAdapter;Lcom/alkemyacademy/challengeingreso/databinding/ItemMovieBinding;)V", "bind", "", "movie", "Lcom/alkemyacademy/challengeingreso/domain/entities/Movie;", "app_debug"})
    public final class MoviesViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        private final com.alkemyacademy.challengeingreso.databinding.ItemMovieBinding binding = null;
        
        public MoviesViewHolder(@org.jetbrains.annotations.NotNull()
        com.alkemyacademy.challengeingreso.databinding.ItemMovieBinding binding) {
            super(null);
        }
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.alkemyacademy.challengeingreso.domain.entities.Movie movie) {
        }
    }
}