package com.alkemyacademy.challengeingreso.presentation.viewmodel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0014\u0010\u0016\u001a\u00020\u00172\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u000e0\rJ\u0006\u0010\u0007\u001a\u00020\u0017J\u0006\u0010\u0019\u001a\u00020\u0017J\u001e\u0010\u001a\u001a\u00020\u00172\u0006\u0010\u001b\u001a\u00020\u00062\u0006\u0010\u001c\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u0006R\u001a\u0010\u0005\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001d\u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\r0\f\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u001a\u0010\u0011\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\r0\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00060\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00060\f\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0010\u00a8\u0006\u001d"}, d2 = {"Lcom/alkemyacademy/challengeingreso/presentation/viewmodel/ViewModelDemo;", "Landroidx/lifecycle/ViewModel;", "moviesUseCases", "Lcom/alkemyacademy/challengeingreso/domain/usecases/MoviesUseCases;", "(Lcom/alkemyacademy/challengeingreso/domain/usecases/MoviesUseCases;)V", "guestId", "", "getGuestId", "()Ljava/lang/String;", "setGuestId", "(Ljava/lang/String;)V", "listMovies", "Landroidx/lifecycle/LiveData;", "", "Lcom/alkemyacademy/challengeingreso/domain/entities/Movie;", "getListMovies", "()Landroidx/lifecycle/LiveData;", "mListMovies", "Landroidx/lifecycle/MutableLiveData;", "mOnError", "onError", "getOnError", "filterMovies", "", "filteredMovies", "getPopularMovies", "postRating", "id", "Body", "app_debug"})
public final class ViewModelDemo extends androidx.lifecycle.ViewModel {
    private final com.alkemyacademy.challengeingreso.domain.usecases.MoviesUseCases moviesUseCases = null;
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.alkemyacademy.challengeingreso.domain.entities.Movie>> mListMovies = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.util.List<com.alkemyacademy.challengeingreso.domain.entities.Movie>> listMovies = null;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String guestId = "";
    private final androidx.lifecycle.MutableLiveData<java.lang.String> mOnError = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.lang.String> onError = null;
    
    public ViewModelDemo(@org.jetbrains.annotations.NotNull()
    com.alkemyacademy.challengeingreso.domain.usecases.MoviesUseCases moviesUseCases) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.List<com.alkemyacademy.challengeingreso.domain.entities.Movie>> getListMovies() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getGuestId() {
        return null;
    }
    
    public final void setGuestId(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.lang.String> getOnError() {
        return null;
    }
    
    public final void getPopularMovies() {
    }
    
    public final void filterMovies(@org.jetbrains.annotations.NotNull()
    java.util.List<com.alkemyacademy.challengeingreso.domain.entities.Movie> filteredMovies) {
    }
    
    public final void postRating(@org.jetbrains.annotations.NotNull()
    java.lang.String id, @org.jetbrains.annotations.NotNull()
    java.lang.String Body, @org.jetbrains.annotations.NotNull()
    java.lang.String guestId) {
    }
    
    public final void getGuestId() {
    }
}