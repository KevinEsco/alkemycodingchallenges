package com.alkemyacademy.challengeingreso.data.remote.datasource;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\bf\u0018\u00002\u00020\u0001J\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0005J\u0017\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00070\u0003H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0005J/\u0010\b\u001a\b\u0012\u0004\u0012\u00020\t0\u00032\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u000b2\u0006\u0010\r\u001a\u00020\u000bH\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000e\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u000f"}, d2 = {"Lcom/alkemyacademy/challengeingreso/data/remote/datasource/IRemoteDataSource;", "", "getGuestId", "Lcom/alkemyacademy/challengeingreso/domain/ObjectResult;", "Lcom/alkemyacademy/challengeingreso/data/remote/entities/responses/ResponseSessionId;", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getPopularMovies", "Lcom/alkemyacademy/challengeingreso/domain/entities/ResponseAppApi;", "postRating", "Lcom/alkemyacademy/challengeingreso/data/remote/entities/responses/ResponseRating;", "id", "", "body", "guestId", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public abstract interface IRemoteDataSource {
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object getPopularMovies(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.alkemyacademy.challengeingreso.domain.ObjectResult<com.alkemyacademy.challengeingreso.domain.entities.ResponseAppApi>> continuation);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object getGuestId(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.alkemyacademy.challengeingreso.domain.ObjectResult<com.alkemyacademy.challengeingreso.data.remote.entities.responses.ResponseSessionId>> continuation);
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object postRating(@org.jetbrains.annotations.NotNull()
    java.lang.String id, @org.jetbrains.annotations.NotNull()
    java.lang.String body, @org.jetbrains.annotations.NotNull()
    java.lang.String guestId, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.alkemyacademy.challengeingreso.domain.ObjectResult<com.alkemyacademy.challengeingreso.data.remote.entities.responses.ResponseRating>> continuation);
}