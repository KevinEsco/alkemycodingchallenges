package com.alkemyacademy.challengeingreso.data.remote.datasource;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0017\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0006J\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u0004H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0006J/\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u00042\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u000e\u001a\u00020\fH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000f\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0010"}, d2 = {"Lcom/alkemyacademy/challengeingreso/data/remote/datasource/RemoteDataSource;", "Lcom/alkemyacademy/challengeingreso/data/remote/datasource/IRemoteDataSource;", "()V", "getGuestId", "Lcom/alkemyacademy/challengeingreso/domain/ObjectResult;", "Lcom/alkemyacademy/challengeingreso/data/remote/entities/responses/ResponseSessionId;", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getPopularMovies", "Lcom/alkemyacademy/challengeingreso/domain/entities/ResponseAppApi;", "postRating", "Lcom/alkemyacademy/challengeingreso/data/remote/entities/responses/ResponseRating;", "id", "", "body", "guestId", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public final class RemoteDataSource implements com.alkemyacademy.challengeingreso.data.remote.datasource.IRemoteDataSource {
    
    public RemoteDataSource() {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object getPopularMovies(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.alkemyacademy.challengeingreso.domain.ObjectResult<com.alkemyacademy.challengeingreso.domain.entities.ResponseAppApi>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object postRating(@org.jetbrains.annotations.NotNull()
    java.lang.String id, @org.jetbrains.annotations.NotNull()
    java.lang.String body, @org.jetbrains.annotations.NotNull()
    java.lang.String guestId, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.alkemyacademy.challengeingreso.domain.ObjectResult<com.alkemyacademy.challengeingreso.data.remote.entities.responses.ResponseRating>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object getGuestId(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.alkemyacademy.challengeingreso.domain.ObjectResult<com.alkemyacademy.challengeingreso.data.remote.entities.responses.ResponseSessionId>> continuation) {
        return null;
    }
}