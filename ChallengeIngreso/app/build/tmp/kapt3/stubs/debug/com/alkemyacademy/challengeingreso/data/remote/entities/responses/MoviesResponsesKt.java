package com.alkemyacademy.challengeingreso.data.remote.entities.responses;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000\u001a\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u0016\u0010\u0000\u001a\b\u0012\u0004\u0012\u00020\u00020\u0001*\b\u0012\u0004\u0012\u00020\u00030\u0001\u001a\n\u0010\u0004\u001a\u00020\u0005*\u00020\u0006\u00a8\u0006\u0007"}, d2 = {"toMoviesList", "", "Lcom/alkemyacademy/challengeingreso/domain/entities/Movie;", "Lcom/alkemyacademy/challengeingreso/data/remote/entities/responses/MovieResponse;", "toResponseAppApi", "Lcom/alkemyacademy/challengeingreso/domain/entities/ResponseAppApi;", "Lcom/alkemyacademy/challengeingreso/data/remote/entities/responses/ResponseApi;", "app_debug"})
public final class MoviesResponsesKt {
    
    @org.jetbrains.annotations.NotNull()
    public static final com.alkemyacademy.challengeingreso.domain.entities.ResponseAppApi toResponseAppApi(@org.jetbrains.annotations.NotNull()
    com.alkemyacademy.challengeingreso.data.remote.entities.responses.ResponseApi $this$toResponseAppApi) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.util.List<com.alkemyacademy.challengeingreso.domain.entities.Movie> toMoviesList(@org.jetbrains.annotations.NotNull()
    java.util.List<com.alkemyacademy.challengeingreso.data.remote.entities.responses.MovieResponse> $this$toMoviesList) {
        return null;
    }
}