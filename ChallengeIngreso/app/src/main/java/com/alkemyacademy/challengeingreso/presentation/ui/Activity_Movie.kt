package com.alkemyacademy.challengeingreso.presentation.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import com.alkemyacademy.challengeingreso.databinding.ActivityMovieBinding
import android.widget.Toast
import androidx.activity.viewModels
import com.alkemyacademy.challengeingreso.data.remote.datasource.RemoteDataSource
import com.alkemyacademy.challengeingreso.data.remote.repository.RemoteRepository
import com.alkemyacademy.challengeingreso.domain.usecases.MoviesUseCases
import com.alkemyacademy.challengeingreso.presentation.viewmodel.ViewModelDemo
import com.alkemyacademy.challengeingreso.presentation.viewmodel.ViewModelFactory
import com.bumptech.glide.Glide


class Activity_Movie : AppCompatActivity() {

    private lateinit var binding: ActivityMovieBinding
    private val viewModel by viewModels<ViewModelDemo>{
        val remoteRepository = RemoteRepository(RemoteDataSource())
        ViewModelFactory(MoviesUseCases(remoteRepository))
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bind()
        val path:String = intent.getStringExtra("posterPath")?.toString()?:"noImage"
        val title:String = intent.getStringExtra("title").toString()
        val id:String = intent.getStringExtra("id").toString()
        val overview:String = intent.getStringExtra("overview").toString()

        viewModel.getGuestId()

        Glide.with(this)
            .load("https://image.tmdb.org/t/p/w500$path")
            .into( this.binding.ivPosterMovie)
        this.binding.tvTitleMovie.text = title
        this.binding.tvInfoMovie.text = overview
        this.binding.btnSubmit.setOnClickListener(){
            var rating = this.binding.rbMovie.rating.toString()
            Toast.makeText(this, "Given Rating: "+rating,
                Toast.LENGTH_SHORT).show()
            viewModel.postRating(id,"{ 'Value': $rating }", viewModel.guestId)


        }
    }

    private fun bind() {
        this.binding = ActivityMovieBinding.inflate(layoutInflater)
        setContentView(this.binding.root)
    }
}