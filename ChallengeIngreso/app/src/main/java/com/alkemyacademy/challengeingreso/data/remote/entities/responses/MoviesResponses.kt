package com.alkemyacademy.challengeingreso.data.remote.entities.responses

import com.alkemyacademy.challengeingreso.domain.entities.Movie
import com.alkemyacademy.challengeingreso.domain.entities.ResponseAppApi

data class ResponseApi(val results: List<MovieResponse>)
data class ResponseSessionId(val guest_session_id: String,val success:String)
data class ResponseRating(val status_code: String, val status_message: String)
data class MovieResponse(val original_title: String, val overview: String, val release_date: String, val poster_path:String, val vote_average:String, val id:String)


fun ResponseApi.toResponseAppApi(): ResponseAppApi = ResponseAppApi(this.results.toMoviesList())
fun List<MovieResponse>.toMoviesList(): List<Movie> = this.map { Movie(it.original_title,it.overview, it.release_date, it.poster_path, it.vote_average, it.id) }
