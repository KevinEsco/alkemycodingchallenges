package com.alkemyacademy.challengeingreso.data.remote

import com.alkemyacademy.challengeingreso.data.remote.entities.responses.ResponseApi
import com.alkemyacademy.challengeingreso.data.remote.entities.responses.ResponseRating
import com.alkemyacademy.challengeingreso.data.remote.entities.responses.ResponseSessionId
import retrofit2.Response
import retrofit2.http.*

interface IServiceApiClient {
    @GET("movie/popular?api_key=4d0ba9d0c1af417434d23f82c8359d1b")
    suspend fun getPopularMovies(): Response<ResponseApi>
    @POST("movie/{id}/rating?api_key=4d0ba9d0c1af417434d23f82c8359d1b")
    suspend fun postRating(@Path("id") id: String, @Body body: String, @Query("guest_session_id") guestId:String ): Response<ResponseRating>
    @GET("authentication/guest_session/new?api_key=4d0ba9d0c1af417434d23f82c8359d1b")
    suspend fun getGuestId():Response<ResponseSessionId>





}