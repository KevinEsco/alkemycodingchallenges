package com.alkemyacademy.challengeingreso.presentation.viewmodel;

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.alkemyacademy.challengeingreso.domain.usecases.MoviesUseCases


class ViewModelFactory(private val moviesUseCases: MoviesUseCases) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T =
        ViewModelDemo(moviesUseCases) as T
}