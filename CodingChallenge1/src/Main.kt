import java.lang.Math.ceil
import java.util.*


data class Parkable(var vehicle: Vehicle){

}

data class Parking(val vehiclesParking: MutableSet<Vehicle>) {
    var RetirosImporte : Pair<Int,Int> = Pair(0,0)
    fun addVehicle(vehicle: Vehicle): Boolean {
        if (vehiclesParking.count() < 20) {
            if (vehiclesParking.add(vehicle)) {
                println("Welcome to AlkeParking")
                return true
            } else {
                println("Sorry, the check-in failed")
                return false
            }

        } else{
            println("Sorry, the check-in failed")
            return false
        }

    }

    fun CheckoutInfo(){
        println("${RetirosImporte.first} vehicles have checked out and have earnings of $${RetirosImporte.second}")
    }
    fun listVehicles(){
        vehiclesParking.forEach({
            println(it.plate)
        })
    }
    fun checkOutVehicle(vehicle: Vehicle){
        if(vehiclesParking.contains(vehicle)){

            vehiclesParking.remove(vehicle)

            OnSuccess(CalculateFee(vehicle.vehicleType,vehicle.parkedTime.toDouble(),!vehicle.DiscountCard.isNullOrBlank()))
        }else{
            OnError()
        }
    }
    fun OnSuccess(Importe: Int){
        RetirosImporte = RetirosImporte.copy(first = RetirosImporte.first + 1,second = RetirosImporte.second + Importe)

        println("Your fee is $$Importe. Come back soon.")
    }
    fun OnError(){
        println("Sorry, the check-out failed")
    }

    fun CalculateFee(type: VehicleType, parkedTime:Double,hasDiscountCard:Boolean): Int {
        //vehicle.parkedTime
        var FeeValue: Int = 0
        var MontoInicial = type.Precio
        FeeValue += MontoInicial
        if ( parkedTime > 120){
            var MinutosExcedidos: Double = parkedTime - 120
            var BloquesExcedidos: Double = MinutosExcedidos / 15
            var BloquesInt = ceil(BloquesExcedidos).toInt()
            FeeValue += (5 * BloquesInt)
        }
        if(hasDiscountCard){
            FeeValue = ceil(FeeValue * 0.85).toInt()
        }
        return FeeValue
    }
}

enum class VehicleType(var Precio: Int) {
    Auto(20),
    Moto(15),
    MiniBus(25),
    Bus(30)

}

data class Vehicle(
    val plate: String,
    val vehicleType: VehicleType,
    var checkInTime: Calendar,
    var DiscountCard: String?
) {
    private val MINUTES_IN_MILISECONDS: Long = 60000
    val parkedTime: Long
        get() = (Calendar.getInstance().timeInMillis - checkInTime.timeInMillis) / MINUTES_IN_MILISECONDS

    override fun equals(other: Any?): Boolean {
        if (other is Vehicle) {
            return this.plate == other.plate
        }
        return super.equals(other)
    }

    override fun hashCode(): Int {
        return this.plate.hashCode()
    }


}

fun main() {
    var Moto = Vehicle("A113WMO", VehicleType.Moto, Calendar.getInstance(), null);
    var Auto = Vehicle("KEX951", VehicleType.Auto, Calendar.getInstance(), null);
    var Bus = Vehicle("WMO111", VehicleType.Bus, Calendar.getInstance(), null);
    var MiniBus = Vehicle("WMO111", VehicleType.MiniBus, Calendar.getInstance(), null);
    var MotoB = Vehicle("A100WMO", VehicleType.Moto, Calendar.getInstance(), null);
    var AutoB = Vehicle("KEX151", VehicleType.Auto, Calendar.getInstance(), null);
    var BusB = Vehicle("WMO113", VehicleType.Bus, Calendar.getInstance(), null);
    var MiniBusB = Vehicle("WMO114", VehicleType.MiniBus, Calendar.getInstance(), "true");
    var MotoC = Vehicle("A200WMO", VehicleType.Moto, Calendar.getInstance(), null);
    var AutoC = Vehicle("KEX351", VehicleType.Auto, Calendar.getInstance(), null);
    var BusC = Vehicle("WMO114", VehicleType.Bus, Calendar.getInstance(), null);
    var MiniBusC = Vehicle("WMO115", VehicleType.MiniBus, Calendar.getInstance(), null);
    var MotoD = Vehicle("A300WMO", VehicleType.Moto, Calendar.getInstance(), null);
    var AutoD = Vehicle("KEX400", VehicleType.Auto, Calendar.getInstance(), null);
    var BusD = Vehicle("WMO116", VehicleType.Bus, Calendar.getInstance(), null);
    var MiniBusD = Vehicle("WMO117", VehicleType.MiniBus, Calendar.getInstance(), null);
    var MotoE = Vehicle("A400WMO", VehicleType.Moto, Calendar.getInstance(), null);
    var AutoE = Vehicle("KEX500", VehicleType.Auto, Calendar.getInstance(), null);
    var BusE = Vehicle("WMO117", VehicleType.Bus, Calendar.getInstance(), null);
    var MiniBusE = Vehicle("WMO118", VehicleType.MiniBus, Calendar.getInstance(), null);
    var MiniBusF = Vehicle("WMO118", VehicleType.MiniBus, Calendar.getInstance(), null);

    var Vehiculos: List<Vehicle> = listOf(
        MiniBusE,
        MiniBusF,
        Moto,
        Auto,
        Bus,
        MiniBus,
        MotoB,
        AutoB,
        BusB,
        MiniBusB,
        MotoC,
        AutoC,
        BusC,
        MiniBusC,
        MotoD,
        AutoD,
        BusD,
        MiniBusD,
        MotoE,
        AutoE,
        MiniBusF,
        BusE
    )
    var EmptySet: MutableSet<Vehicle> = mutableSetOf()
    val Estacionamiento = Parking(EmptySet)
    println(Estacionamiento.vehiclesParking.contains(Moto))
    for (vehiculo in Vehiculos){
      Estacionamiento.addVehicle(vehiculo)
    }
    Estacionamiento.checkOutVehicle(Moto)
    Estacionamiento.listVehicles()
    Estacionamiento.CheckoutInfo()

}
